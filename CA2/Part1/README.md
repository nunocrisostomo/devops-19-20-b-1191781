![gradle-logo-horizontal-small.png](https://bitbucket.org/repo/8XqLBk4/images/1121176611-gradle-logo-horizontal-small.png "DevOps - Class Assignment 2 \(CA2\) - Part 1")

# **DevOps - Class Assignment 2 (CA2) - Part 1** #

*"The goal of the Part 1 of this assignment is to practice gradle using a very simple
example"*
-------------
## **Steps taken to complete the assignment** ##

### Project structure and first commit ###
**1)** Start by creating the folder structure - CA2\Part1

**2)** Download the project files from [https://bitbucket.org/luisnogueira/gradle_basic_demo/](https://bitbucket.org/luisnogueira/gradle_basic_demo/)

**3)** Make and push the first commit of the assignment

```
#!git

 git add CA2
 git commit -m "Fix #9 CA2 - First commit of project files"
 git push
```
-------------
### Run and test the chat application ###

**4)** Build the gradle project (.jar file)

```
#!cli

 gradlew build
```

**5)** Open a terminal and execute the following command from the project's root directory on port 59001

```
#!cli
 java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001

```
**6)** Open another terminal and execute the gradle **runClient** task from the project's root directory and it'll connect to the Chat Server on port 59001 as defined in the task args.

```
#!cli
 gradlew runClient

```
-------------
### Add a new task to execute the server ###
**7)** Create a task named **runServer** in file ***build.gradle*** with the following code:

```
#!gradle
task runServer(type:JavaExec, dependsOn: classes){
    group = "DevOps"
    description = "Launches a chat server on localhost:59001"

    classpath = sourceSets.main.runtimeClasspath

    main = 'basic_demo.ChatServerApp'

    args '59001'
}

```
**8)** Commit and push the changes to the repository

```
#!git

 git commit -m "Fix #10 CA2 - Add a new task to execute the server"
 git push
```
-------------
### Add a simple unit test and update the gradle script so that it is able to execute the test ###

**9)** Add junit4.12 dependency to **build.gradle** in *dependencies{}*

```
#!gradle

 dependencies {
     (...)
     implementation 'junit:junit:4.12'
 }
```
**10)** Create folder structure for test

```
#!cli

  mkdir src\main\test\java\basic_demo
```
**11)** Create test class file **AppTest.class** with the code provided

```
#!java
   package basic_demo;

   import org.junit.Test;
   import static org.junit.Assert.*;

   public class AppTest {
       @Test
       public void testAppHasAGreeting() {
           App classUnderTest = new App();
           assertNotNull("app should have a greeting", classUnderTest.getGreeting());
       }
   }

```
**12)** Run gradle test task

```
#!cli

   gradlew clean test --info
```
Used clean to ensure that the buildDir folder is removed and --info command-line option to activate verbose/log on screen. Doing that ensured that the tests were being run correctly because in the first tries they weren't.

**13)** Open *build/reports/tests/test/**index.html*** on browser to see tests results

**14)** Commit and push the changes to the repository

```
#!git

   git add src/test
   git commit -m "Fix #11 CA2 - Add unit test and update gradle script with junit dependency"
   git push
```
-------------
### Add a new task of type Copy to be used to make a backup of the sources of the application ###
**15)** Create a task named **backupSourceCode** in file ***build.gradle*** with the following code:

```
#!gradle
   task backupSourceCode(type: Copy) {
       from 'src'
       into 'backup'
   }

```
**16)** Test **backupSourceCode** task

```
#!cli

   gradlew backupSourceCode
```
**17)** Commit and push the changes to the repository

```
#!git

   git commit -m "Fix #12 CA2 - Add a new task of type Copy to be used to make a backup of the source code"
   git push
```
-------------
### Add a new task of type Zip to be used to make an archive (i.e., zip file) of the sources of the application ###
**18)** Create a task named **zipSourceCode** in file ***build.gradle*** with the following code:

```
#!gradle
    task zipSourceCode(type: Zip) {
        archiveFileName.set('source_code_backup_'+ getDate() +'.zip')
        from(files('./src'))
    }

    def getDate() {
        return new Date().format('yyyy-MM-dd-HHmm')
    }

```
**19)** Test **zipSourceCode** task

```
#!cli

   gradlew zipSourceCode
```
The zip file will be created in *build/distributions*

**20)** Commit and push the changes to the repository

```
#!git

   git commit -m "Fix #13 CA2 - Add a new task of type Zip to be used to make an archive of the source code"
   git push
```
-------------
### At the end of the part 1 of this assignment mark your repository with the tag ca2-part1 ###

**21)** Create ca2-part1 tag and push it to the repository

```
#!git

   git tag ca2-part1
   git push origin ca2-part1
```
-------------