package com.greglturnquist.payroll;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {
    @Test
    void getFirstName() {
        //Arrange
        Employee employee = new Employee("Albertina", "Dias", "Best employee");

        String expected = "Albertina";

        //Act
        String result = employee.getFirstName();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void setFirstName() {
        //Arrange
        Employee employee = new Employee("Albertina", "Dias", "Best employee");

        employee.setFirstName("Rodolfina");

        String expected = "Rodolfina";

        //Act
        String result = employee.getFirstName();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void testToString() {
        //Arrange
        Employee employee = new Employee("Albertina", "Dias", "Best employee");

        String expected = "Employee{id=null, firstName='Albertina', lastName='Dias', description='Best employee'}";

        //Act
        String result = employee.toString();

        //Assert
        assertEquals(expected, result);
    }
}