![Apache-Ant-logo-small.png](https://bitbucket.org/repo/8XqLBk4/images/1187016021-Apache-Ant-logo-small.png)

# **DevOps - Class Assignment 2 (CA2)**
## **Alternative - Apache Ant - Part 1 and Part 2** ##

*"The goal of the Part 1 of this assignment is to use Ant as an alternative to Gradle"*

-------------
## Apache Ant vs Gradle ##

I chose Ant as an alternate to Gradle because I wanted to find out and understand the evolution of build automation tools.

Ant was created to enhance the build process of the Make build tool. It's based on Java and uses XML as the base for build scripts.
Comparing with Gradle, Ant allows more freedom within the build process but with it comes more complexity due to the allowed and different approaches and since it uses XML rather than Groovy the build speed is improved because it only has to parse the script file. Gradle on the opposite hand has got to recompile the Groovy script for each build but features a lot more flexibility.

Ant doesn't have a dependency manager (you need to use Ivy) and can't resolve automatically the dependencies needed to create the project. The scripts being based on XML are more verbose than Groovy-based Gradle scripts.

Below may be a table resuming the differences of the 2 build tools:

|Ant|Gradle|
|--- |--- |
|Ant is a Java-based build automation tool.|Gradle is a Groovy-based build automation tool.|
|It uses XML files to determine build scripts.|It uses DSL (Domain Specific Language) instead of XML (Extensible Markup Language) to define build scripts|
|It is developed to overcome the drawbacks of the Make build tool of Unix.|Gradle is developed to overcome the drawbacks of Maven and Ant.|
|It does not impose any coding convention/language.|Gradle plugins are coded in Java or Groovy programming language.|
|It does not impose a project structure|Gradle provides a structured build.|
|In Ant, the IDE integration is slightly complex than Gradle.|Gradle provides comfortable support for IDE integration.|
|It is less flexible than Gradle.|Gradle is more standardized than Ant in case of flexibility.|
|It does not support multi-project build.|Supports multi-project build.|

###### Source: https://www.javatpoint.com/gradle-vs-ant ######

In conclusion, Ant allows you to control every step of the build process, offers a simple XML script structure and doesn't force you to use any coding convention.
But without native dependency management, verbose scripts and allowing unstructured project as major drawbacks, it comes out as a less likable option comparing to Gradle.

-------------
## **CA2 Implementation using Ant**

The following steps will describe Part 1 and Part 2 of the assigment without the implementation of the Webpack build part, as agreed with professor Alexandre Bragança.

For the sake of simplicity, all of the Git steps will be omitted.

-------------
## Part 1

### **Run and test the chat application** ###

**1)** Build the ant project (.jar file)

```
#!xml
    <target name="compile" depends="prepare">
        <javac srcdir="${src.dir}" destdir="${classes.dir}" includeantruntime="false">
            <classpath>
                <fileset dir="${lib.dir}">
                    <include name="*.jar"/>
                </fileset>
            </classpath>
            <zipgroupfileset dir="${lib.dir}" includes="**/*.jar"/>
        </javac>
    </target>
    <target name="jar" depends="compile">
        <jar destfile="${jar.dir}/${ant.project.name}.jar" basedir="${classes.dir}">
            <manifest>
                <attribute name="Main-Class" value="${server-class}"/>
            </manifest>
        </jar>
    </target>
```

```
#!cli

 ant jar
```

**2)** Open a terminal and execute the following command from the project's root directory on port 59001

```
#!cli
 java -cp build/jar/DevOpsCA2.jar basic_demo.ChatServerApp 59001

```
**3)** Open another terminal and execute the gradle **runClient** task from the project's root directory and it'll connect to the Chat Server on port 59001 as defined in the task args.

```
#!xml

    <target name="runClient" depends="compile">
        <java fork="true" classname="${client-class}">
            <classpath>
                <pathelement location="${jar.dir}/${ant.project.name}.jar"/>
            </classpath>
            <arg value="localhost"/>
            <arg value="59001"/>
        </java>
    </target>
```

```
#!cli
 ant runClient

```
-------------
### Add a new task to execute the server ###
**4)** Create a task named **runServer** in file ***build.xml*** with the following code:

```
#!xml
    <target name="runServer" depends="compile">
        <java jar="${jar.dir}/${ant.project.name}.jar" fork="true">
            <arg value="59001"/>
        </java>
    </target>

```

-------------
### Add a simple unit test and update the gradle script so that it is able to execute the test ###

**5)** Add junit4.12 dependency to **build.xml** in *junit -> classpath* and define *batchtest* to run all test found.

```
#!xml
    <junit printsummary="yes" fork="yes" haltonfailure="no">
        <classpath>
            <fileset dir="${lib.dir}">
                <include name="ant-junit.jar"/>
            </fileset>
            <fileset dir="${build.dir}/jar">
                <include name="DevOpsCA2.jar"/>
            </fileset>
        </classpath>
        <formatter type="plain"/>
        <batchtest fork="yes" todir="${reports.dir}">
            <fileset dir="${src.dir}">
                <include name="**/*Test*.java"/>
            </fileset>
        </batchtest>
    </junit>
```
```
#!cli

   ant compile
```

-------------
### Add a new task of type Copy to be used to make a backup of the sources of the application ###
**6)** Create a task named **backupSourceCode** in file ***build.xml*** with the following code:

```
#!xml
    <target name="copySourceCode">
        <mkdir dir="backup"/>
        <mkdir dir="backup/src"/>
        <copy todir="backup/src">
            <fileset dir="${src.dir}" includes="**"/>
        </copy>
    </target>

```
**7)** Test **backupSourceCode** task

```
#!cli

   ant backupSourceCode
```

-------------
### Add a new task of type Zip to be used to make an archive (i.e., zip file) of the sources of the application ###
**7)** Create a task named **zipSourceCode** in file ***build.xml*** with the following code:

```
#!gradle

    <target name="zipSourceCode">
        <mkdir dir="backup"/>
        <zip destfile="backup/sourceBackup.zip"
             basedir="${src.dir}"/>
    </target>
```
**8)** Test **zipSourceCode** task

```
#!cli

   ant zipSourceCode
```
The zip file will be created in *backup/

-------------