![vagrant-small.png](https://bitbucket.org/repo/8XqLBk4/images/438505477-vagrant-small.png "DevOps - Class Assignment 3 \(CA3\) - Part 2")

# **DevOps - Class Assignment 3 (CA3) - Part 2** #

*"The goal of Part 2 of this assignment is to use Vagrant to setup a virtual environment
  to execute the tutorial spring boot application, gradle "basic" version (developed in
  CA2, Part2)"*
-------------
## **Steps taken to complete the assignment** ##

The first two steps (1,2) of the CA3 Part 2, are only for the download and analysis of the Vagrantfile. 

The Bitbucket repository was made public, in *Repository Access > Repository details*, for ease of access when cloning it with vagrant.

**3)** Copy this Vagrantfile to your repository (inside the folder for this assignment).

     Download the repository files and copy the Vagrantfile file to the CA3\Part2 directory 

**4)** Update the Vagrantfile configuration so that it uses your own gradle version of the
       spring application.
       
Change line 70 to 76 of Vagrantfile to the following instructions:
```
git clone https://nunocrisostomo@bitbucket.org/nunocrisostomo/devops-19-20-b-1191781.git
cd devops-19-20-b-1191781/CA2/Part2
chmod +x gradlew
sudo ./gradlew clean build
# To deploy the war file to tomcat8 do the following command:
sudo cp build/libs/demo-0.0.1-SNAPSHOT.war /var/lib/tomcat8/webapps

# Added this line because it was the only way to make tomcat read the newly created war file 
sudo service tomcat8 restart
```

**5)** Check https://bitbucket.org/atb/tut-basic-gradle to see the changes
       necessary so that the spring application uses the H2 server in the db VM. Replicate
       the changes in your own version of the spring application.
       
Open **build.gradle** file and add the following lines:
```
plugins {
	(...)
	id 'war'
}
dependencies {
    (...)
    runtimeOnly 'com.h2database:h2'
	providedRuntime 'org.springframework.boot:spring-boot-starter-tomcat'
}
```

Create java class **ServletInitializer.java** with the following contents:

```
package com.greglturnquist.payroll;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

public class ServletInitializer extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(ReactAndSpringDataRestApplication.class);
    }

}
```

Update **application.properties** file in **src/java/main/resources** with the following lines:
```
server.servlet.context-path=/demo-0.0.1-SNAPSHOT
spring.data.rest.base-path=/api
#spring.datasource.url=jdbc:h2:mem:jpadb
# In the following settings the h2 file is created in /home/vagrant folder
spring.datasource.url=jdbc:h2:tcp://192.168.33.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
spring.datasource.driverClassName=org.h2.Driver
spring.datasource.username=sa
spring.datasource.password=
spring.jpa.database-platform=org.hibernate.dialect.H2Dialect
# So that spring will no drop de database on every execution.
spring.jpa.hibernate.ddl-auto=update
spring.h2.console.enabled=true
spring.h2.console.path=/h2-console
spring.h2.console.settings.web-allow-others=true
```

Update **app.js** file in **src/main/js/app.js** with the following lines:
```
	componentDidMount() { // <2>
		client({method: 'GET', path: '/demo-0.0.1-SNAPSHOT/api/employees'}).done(response => {
			this.setState({employees: response.entity._embedded.employees});
		});
	}
```

Add the file, commit and push the changes:
```
git add build.gradle

git commit -m "Fix #22 CA3 - Part 2 - Update build.gradle with H2 DB an war dependencies"

git add src/main/java/com/greglturnquist/payroll/ServletInitializer.java

git commit -m "Fix #23 CA3 - Part 2 - Create ServletInitializer class"

git add >git add src/main/resources/application.properties

git commit -m "Fix #26 CA3 - Part 2 - Update build.gradle and application.properties file to use H2 and the correct api path"

git add src/main/js/app.js

git commit -m "Fix #27 Update app.js to reflect the real path to the API"

git push
```

Then run the file with:

```
vagrant up
```

After Vagrant ends creation of the VMs open the following address in your internet browser:

```
http://192.168.33.10:8080/demo-0.0.1-SNAPSHOT/
```

**6)** At the end of the part 2 of this assignment mark your repository with the tag
       ca3-part2.

```
git add README.md
git add Vagrantfile

git commit -m "Fix #28 Adds README.md of Class Assignment 3 (CA3) Part 2 and Vagrantfile" 

git push

git tag ca3-part2

git push origin ca3-part2
```