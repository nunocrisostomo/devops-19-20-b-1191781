![vagrant-small.png](https://bitbucket.org/repo/8XqLBk4/images/438505477-vagrant-small.png "DevOps - Class Assignment 3 \(CA3\) - Part 2")

# **DevOps - Class Assignment 3 (CA3)** #

*For this assignment you should present an alternative technological solution for the
 virtualization tool (i.e., an hypervisor alternative to VirtualBox).*
-------------
## Alternative - VMWare Workstation ##

For this class assignment I chose WMWare Workstation as an alternative to VirtualBox.  

The main problem that I encountered was that to use any of the VMWare ecosystem options, the user has to buy a plugin from the software company that develops vagrant, Hashicorp.  
Everything described below is purely theoretical because I couldn't test it. I can't compare the two hypervisor solutions completely but I can say the following:

* VMWare Workstation is a paid solution and VirtualBox is free;
* To use VMWare you have to pay an additional fee for the vagrant plugin;
* From the research made, in VMWare Workstation the Vagrant VMs are not listed in the GUI as it is in VirtuaBox;
* There are some problems when trying to destroy the Vagrant machines;
* The WMWare provider is well supported and generally more stable and performant than VirtualBox.    

## **Steps taken to complete the assignment** ##

**1)** Install the VMWare Plugin and run:
```
vagrant plugin license vagrant-vmware-desktop ~/license.lic
```

**2)** The Vagranfile was edited to change the hypervisor from VirtualBox to VMWare Workstation:
```
# See: https://manski.net/2016/09/vagrant-multi-machine-tutorial/
# for information about machine names on private network
Vagrant.configure("2") do |config|
  config.vm.box = "envimation/ubuntu-xenial"

  # This provision is common for both VMs
  config.vm.provision "shell", inline: <<-SHELL
    sudo apt-get update -y
    sudo apt-get install iputils-ping -y
    sudo apt-get install -y avahi-daemon libnss-mdns
    sudo apt-get install -y unzip
    sudo apt-get install openjdk-8-jdk-headless -y
    # ifconfig
  SHELL

  #============
  # Configurations specific to the database VM
  config.vm.define "db" do |db|
    db.vm.box = "envimation/ubuntu-xenial"
    db.vm.hostname = "db"
    db.vm.network "private_network", ip: "192.168.33.11"

    # We want to access H2 console from the host using port 8082
    # We want to connet to the H2 server using port 9092
    db.vm.network "forwarded_port", guest: 8082, host: 8082
    db.vm.network "forwarded_port", guest: 9092, host: 9092

    # We need to download H2
    db.vm.provision "shell", inline: <<-SHELL
      wget https://repo1.maven.org/maven2/com/h2database/h2/1.4.200/h2-1.4.200.jar
    SHELL

    # The following provision shell will run ALWAYS so that we can execute the H2 server process
    # This could be done in a different way, for instance, setiing H2 as as service, like in the following link:
    # How to setup java as a service in ubuntu: http://www.jcgonzalez.com/ubuntu-16-java-service-wrapper-example
    #
    # To connect to H2 use: jdbc:h2:tcp://192.168.33.11:9092/./jpadb
    db.vm.provision "shell", :run => 'always', inline: <<-SHELL
      java -cp ./h2*.jar org.h2.tools.Server -web -webAllowOthers -tcp -tcpAllowOthers -ifNotExists > ~/out.txt &
    SHELL
  end

  #============
  # Configurations specific to the webserver VM
  config.vm.define "web" do |web|
    web.vm.box = "envimation/ubuntu-xenial"
    web.vm.hostname = "web"
    web.vm.network "private_network", ip: "192.168.33.10"

    # We set more ram memmory for this VM
    web.vm.provider "vmware_desktop" do |v|
      v.vmx["memsize"] = "1024"
    end

    # We want to access tomcat from the host using port 8080
    web.vm.network "forwarded_port", guest: 8080, host: 8080

    web.vm.provision "shell", inline: <<-SHELL
      sudo apt-get install git -y
      sudo apt-get install nodejs -y
      sudo apt-get install npm -y
      sudo ln -s /usr/bin/nodejs /usr/bin/node
      sudo apt install tomcat8 -y
      sudo apt install tomcat8-admin -y
      # If you want to access Tomcat admin web page do the following:
      # Edit /etc/tomcat8/tomcat-users.xml
      # uncomment tomcat-users and add manager-gui to tomcat user

      # Change the following command to clone your own repository!
      git clone https://nunocrisostomo@bitbucket.org/nunocrisostomo/devops-19-20-b-1191781.git
      cd devops-19-20-b-1191781/CA2/Part2
      chmod +x gradlew
      sudo ./gradlew clean build
      # To deploy the war file to tomcat8 do the following command:
      sudo cp build/libs/demo-0.0.1-SNAPSHOT.war /var/lib/tomcat8/webapps
      sudo service tomcat8 restart
    SHELL
  end
end
```

Then run with:
```
vagrant up --provider vmware_desktop 
```