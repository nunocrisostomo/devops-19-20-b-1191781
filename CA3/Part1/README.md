![virtualbox-small.png](https://bitbucket.org/repo/8XqLBk4/images/148344390-virtualbox-small.png "DevOps - Class Assignment 3 \(CA3\) - Part 1")

# **DevOps - Class Assignment 3 (CA3) - Part 1** #

*"The goal of the Part 1 of this assignment is to practice with VirtualBox using the same
  projects from the previous assignments but now inside a VirtualBox VM with Ubuntu"*
-------------
## **Steps taken to complete the assignment** ##

**1)** You should start by creating your VM as described in the lecture

     The VM was created using the instructions on the devops05.pdf file.

**2)** You should clone your individual repository inside the VM

```
git clone https://nunocrisostomo@bitbucket.org/nunocrisostomo/devops-19-20-b-1191781.git
```

**3)** You should try to build and execute the spring boot tutorial basic project and the
       gradle_basic_demo project (from the previous assignments)      

```
sudo apt install maven
sudo apt install gradle
```

>*Git and JDK were already installed during the VM creation and configuration.*

**4)** For web projects you should access the web applications from the browser in your
       host machine (i.e., the "real" machine)  

```
./gradlew bootRun
```

And then in the host machine, opened the http://192.168.56.100:8080/ URL in a browser.

### **Problems found until the build and running tasks were successful:** ###

##### This part of the report answers the questions 5 and 6 of the assigment: #####

***Running ./gradlew***

>-bash: ./gradlew: Permission denied

Gave execution permissions to the file gradlew in Part 1 and Part 2

```
chmod +x gradlew
```

***Running ./gradlew runClient in Part 1***

>No X11 DISPLAY variable was set, but this program performed an operation which requires it.

This error appeared when trying to run the runClient task, and means that I'm running a headless version of Ubuntu Server (console based and in this context simply means without a graphical display).  
This is why we can run the runServer task that has a console based execution and we cannot run the runClient task.

***Running ./gradlew build in Part 2***

>Could not target platform: 'Java SE 11' using tool chain: 'JDK 8 (1.8)'

Had to change the line below, from 11 to 1.8, in the build.gradle file to correct this error: 
```
sourceCompatibility = '1.8'
```

**7)** At the end of the part 1 of this assignment mark your repository with the tag
       ca3-part1. 
       
```
git add CA3/Part1/README.md

git commit -m "Fix #21 Adds README.md of Class Assignment 3 (CA3)" 

git push

git tag ca3-part1

git push origin ca3-part1
```