# DEVOPS - Class Assignment 1 #

## Para que serve esta pasta repositório? ##

Esta pasta do repositório tem como objetivo apresentar os passos dados para implementar a Class Assignment 1 (CA1)
da disciplina DEVOPS.

## Passos realizados na implementação da CA1 ##

1- Clone repository

```
#!git

git clone https://nunocrisostomo@bitbucket.org/nunocrisostomo/devops-19-20-b-1191781.git
```

2 - *Tag the initial version as v1.20*  

```
#!git
   
   git tag -a v1.2.0 -m "Changed the version number"
   git push origin v1.2.0
   
```
3- *You should create a branch called email-field*  
    
```
#!git

    git branch email-field  
    git push --set-upstream origin email-field
```

4- *You should add support for email field*:

* Editar o ficheiro **Employee.java**
* Acrescentar atributo de tipo String à classe, com o nome email
* Acrescentar o atributo como parâmetro do construtor e atribuir o valor ao atributo
* Atualizar os overrides dos métodos equals(), hashCode() e toString() com o novo atributo  
   
* Editar o ficheiro **DatabaseLoader.java**
* Atualizar a instanciação do *employee* para conter o novo parâmetro email 
   
* Editar o ficheiro **app.js**
* Acrescentar o header, \<th/>, para o email na classe EmployeeList, no método render()
* Acrescentar a prop, \<td>{this.props.employee.email}\</td>, no método render(), da classe Employee 
    
* Criar a estrutura de packages para Test (src/test/java/com/gregturnquist/payroll)
* Criar ficheiro EmployeeTest.java
* Implementar testes para verificação _Null_ e _Empty_ dos métodos usados no construtor (setFirstName(), setLastName() e setEmail())
   
5- Merge _email-field branch_ with _master branch_
     
```
#!git

     git checkout master
     git merge email-field
     git push
```
     
6- Adicionar a tag v1.3.0

```
#!git
      git tag -a v1.3.0 -m "Support for email field"
      git push origin v1.3.0
```
      
7-  _You should also create branches for fixing bugs (e.g., "fix-invalid-email")_:
 * Criar a branch fix-invalid-email:
      
```
#!git

       git branch fix-invalid-email  
       git checkout fix-invalid-email
```
       
*  Implementar verificação do email, via Regex, ao método setEmail
*  Implementar testes para verificação da estrutura do email 
*  Merge _email-field branch_ with _master branch_
      
```
#!git

      git checkout master
      git merge fix-invalid-email
      git push
```
  
8- Adicionar a tag v1.3.0:

```
#!git

      git tag -a v1.3.1 -m "Checks email string structure"
      git push origin v1.3.1
```
      
9- _At the end of the assignment mark your repository with the tag ca1._:
 Adicionar a tag ca1
       
```
#!git

       git tag -a ca1 -m "CA1 finished"
       git push origin ca1
```

---
# DEVOPS - Class Assignment 1 - Mercurial

## Mercurial vs Git ##

O Mercurial e o Git iniciaram o seu desenvolvimento em Abril de 2005, quando o criador do _BitKeeper_ (_software_ que era usado para o controlo de versões do kernel Linux), removeu o acesso grátis à ferramenta.

Ambos são _Distributed version control systems_ (DVCS) que permitem aos programadores fazerem o _download_ de um repositório, realizarem alterações e efetuar o _upload_ para um servidor central. Isto possibilita fundir o código numa única solução e depois de testado, colocá-lo em produção.

As duas soluções, à superficie, fazem o mesmo mas existem diferenças no seu funcionamento.

O Mercurial é mais simples de utilizar, tem uma sintaxe mais acessível e documentação fácil de perceber. O Git, em termos de utilização, é mais complexo porque oferece mais comandos com muitas opções, e a documentação é não é tão acessível quanto a do Mercurial.

Em termos de _branching_, as duas opções funcionam de forma distinta. O Git oferece um modelo mais eficiente em que as _branches_ são apenas referências para um _commit_. Pode-se criar, eliminar e alterar uma _branch_ em qualquer altura, sem afetar _commits_ usando e uma _staging area_ (que o Mercurial não oferece).

_Branching_ no Mercurial não segue o mesmo sentido. Neste uma _branch_ é uma linha de _changesets_ consecutivos. Estes _changesets_ são um conjunto completo de alterações feitas a um ficheiro num reposítório. Como a _branch_ é embebida num _commit_, estas não podem ser eliminadas por estariamos a alterar o histórico. Para se conseguir trabalhar numa forma semelhante ao Git, pode-se utilizar _Bookmarks_ (que é o método aplicado na implementação desta CA).

Como anteriormente referido, o Mercurial não oferece uma _staging area_ nem indexação antes de um _commit_.

Em conclusão, e apesar da breve utlização das duas ferramentas, estas são muito semelhantes e cada uma oferece uma aproximação ligeiramente diferente ao controlo de versões de _software_.
Tanto uma como a outra, foram para mim, acessíveis mas nota-se que a curva de aprendizagem do Git é um pouco superior mas oferece mais flexibilidade depois de se interiorizar todos conceitos.
 

## Implementação da CA1 em Mercurial
O repositório da implementação está alojado no site [HelixTeamHub](https://helixteamhub.cloud/login), a partir do qual foi enviado um convite aos docentes para criarem uma conta e poderem visualizar todo o projeto.

Caso não o consigam, enviem por favor, um email para 1191781@isep.ipp.pt.

1- Clone repository

```
#!hg

hg clone https://1191781isepipppt@helixteamhub.cloud/dry-speakers-1090/projects/devops-19-20-b-1191781/repositories/mercurial/devops-19-20-B-1191781
```

2- *Tag the initial version as v1.2.0*  
   
```
#!hg

   hg tag v1.2.0 -m "Version 1.2.0"
   hg push
```
   
3- *You should create a branch called email-field*  
* Usar hg bookmark porque é o mais próximo do git branch que existe: 
    
```
#!hg

    hg bookmark main
    hg bookmark email-field
    hg update email-field
```
    
4- *You should add support for email field*:  


* Editar o ficheiro **Employee.java**
* Acrescentar atributo de tipo String à classe, com o nome email
* Acrescentar o atributo como parâmetro do construtor e atribuir o valor ao atributo
* Atualizar os overrides dos métodos equals(), hashCode() e toString() com o novo atributo  
   
* Editar o ficheiro **DatabaseLoader.java**
* Atualizar a instanciação do *employee* para conter o novo parâmetro email 
   
* Editar o ficheiro **app.js**
* Acrescentar o header, \<th/>, para o email na classe EmployeeList, no método render()
* Acrescentar a prop, \<td>{this.props.employee.email}\</td>, no método render(), da classe Employee 
    
* Criar a estrutura de packages para Test (src/test/java/com/gregturnquist/payroll)
* Criar ficheiro EmployeeTest.java
* Implementar testes para verificação _Null_ e _Empty_ dos métodos usados no construtor (setFirstName(), setLastName() e setEmail())
   
5- Merge _email-field bookmark_ with _main bookmark_

```
#!hg

     hg update -C main
     hg update
```
     
6- Adicionar a tag v1.3.0
      
```
#!hg

      hg tag v1.3.0 -m "Version 1.3.0" 
      hg push
```

7- _You should also create branches for fixing bugs (e.g., "fix-invalid-email")_:
* Criar a bookmark fix-invalid-email:
       

```
#!hg

       hg bookmark fix-invalid-email
       hg update fix-invalid-email
```

*  Implementar verificação do email, via Regex, ao método setEmail
*  Implementar testes para verificação da estrutura do email 
*  Merge _fix-invalid-email bookmark_ with _main bookmark_

```
#!hg
      
      hg update -C main
      hg update
```      
8- Adicionar a tag v1.3.0:
      
```
#!hg

      hg tag v1.3.1 -m "Checks email string structure"
      hg push
```
9- Adicionar a tag ca1:
      
```
#!hg

      hg tag ca1 -m "CA1 finished"
      hg push
```
       