package com.greglturnquist.payroll;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {
//"Frodo", "Baggins", "ring bearer", "Main character", "frodie@lotr.org"
    @DisplayName("setFirstName() - Null exception check")
    @Test
    void setFirstNameNull() {
        assertThrows(NullPointerException.class, ()->{
            Employee employee = new Employee(null, "Baggins", "ring bearer", "Main character", "frodie@lotr.org");
        });
    }

    @DisplayName("setFirstName() - Empty String check")
    @Test
    void setFirstNameEmptyString() {
        assertThrows(IllegalArgumentException.class, ()->{
            Employee employee = new Employee(" ", "Baggins", "ring bearer", "Main character", "frodie@lotr.org");
        });
    }

    @DisplayName("setLastName() - Null exception check")
    @Test
    void setLastNameNull() {
        assertThrows(NullPointerException.class, ()->{
            Employee employee = new Employee( "Frodo", null, "ring bearer", "Main character", "frodie@lotr.org");
        });
    }

    @DisplayName("setLastName() - Empty String check")
    @Test
    void setLastNameEmptyString() {
        assertThrows(IllegalArgumentException.class, ()->{
            Employee employee = new Employee("Frodo", " ", "ring bearer", "Main character", "frodie@lotr.org");
        });
    }

    @DisplayName("setEmail() - Null exception check")
    @Test
    void setEmailNull() {
        assertThrows(NullPointerException.class, ()->{
            Employee employee = new Employee( "Frodo", "Baggins", "ring bearer", "Main character", null);
        });
    }

    @DisplayName("setEmail() - Empty String check")
    @Test
    void setEmailEmptyString() {
        assertThrows(IllegalArgumentException.class, ()->{
            Employee employee = new Employee("Frodo", "Baggins", "ring bearer", "Main character", "");
        });
    }

    @DisplayName("setEmail() - Invalid email without name")
    @Test
    void setEmailWithoutName() {
        assertThrows(IllegalArgumentException.class, ()->{
            Employee employee = new Employee("Frodo", "Baggins", "ring bearer", "Main character", "@lotr.org");
        });
    }

    @DisplayName("setEmail() - Invalid email without @")
    @Test
    void setEmailWithoutAtSymbol() {
        assertThrows(IllegalArgumentException.class, ()->{
            Employee employee = new Employee("Frodo", "Baggins", "ring bearer", "Main character", "frodielotr.org");
        });
    }

    @DisplayName("setEmail() - Invalid email without domain")
    @Test
    void setEmailWithoutDomain() {
        assertThrows(IllegalArgumentException.class, ()->{
            Employee employee = new Employee("Frodo", "Baggins", "ring bearer", "Main character", "frodie@");
        });
    }
}