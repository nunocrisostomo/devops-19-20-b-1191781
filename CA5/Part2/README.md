![jenkins-logo.png](https://bitbucket.org/repo/8XqLBk4/images/581662723-jenkins-logo.png "DevOps - Class Assignment 5")

# DevOps - Class Assignment 5 (CA5) - Part 2 #

*"The goal of the Part 2 of this assignment is to create a pipeline in Jenkins to build the tutorial spring boot application, gradle "basic" version (developed in CA2, Part2)"*
-------------
## **Steps taken to complete the assignment** ##

*For the sake of simplicity, some git commands are not shown*

**1)** Create Jenkinsfile and open it, paste the code below:

```
pipeline {
    agent any
    stages {

    }
}
```

**2)**  Add *Checkout* as the first stage of the Pipeline:

*"Checkout. To checkout the code form the repository"*

```
    stage('Checkout') {
        steps {
            echo 'Checking out...'
            git credentialsId: 'nfc-bitbucket-credentials', url:'https://nunocrisostomo@bitbucket.org/nunocrisostomo/devops-19-20-b-1191781.git'
        }
    }
```

**3)**  Add *Assemble* as the second stage of the Pipeline:

*"Assemble. Compiles and Produces the archive files with the application. Do not use the build task of gradle (because it also executes the tests)!"*

```
    stage('Assemble') {
        steps {
            echo 'Assembling...'
            dir('CA2/Part2'){
                bat 'gradlew clean assemble'
            }
        }
    }
```

**4)**  Add *Test* as the third stage of the Pipeline:

*"Test. Executes the Unit Tests and publish in Jenkins the Test results. See the junit step for further information on how to archive/publish test results."*

```
    stage('Test') {
        steps {
            echo 'Running tests...'
            dir('CA2/Part2'){
                bat 'gradlew test'
                junit 'build/test-results/test/*.xml'
            }
        }
    }
```
**5)**  Add *Javadoc* as the third stage of the Pipeline:

*"Javadoc. Generates the javadoc of the project and publish it in Jenkins"*

Open the build.gradle file, in the project CA2 Part 2, and then add the following lines at the end:

```
task packageJavadoc(type: Jar, dependsOn: 'javadoc') {
    from javadoc.destinationDir
    classifier = 'javadoc'
}
artifacts {
    archives packageJavadoc
}
```
_This will generate the Javadoc HTML._

Add the following code to the Jenkinsfile to publish the Javadoc in Jenkins:

```
    stage('Javadoc') {
        steps {
            echo 'Generating Javadoc...'
            dir('CA2/Part2'){
                bat 'gradlew javadoc'
                publishHTML (target: [
                      allowMissing: false,
                      alwaysLinkToLastBuild: true,
                      keepAll: true,
                      reportDir: 'build/docs/javadoc',
                      reportFiles: 'index.html',
                      reportName: "Javadoc"
                    ])
            }

        }
    }
```

_The Javadoc option will appear in the side menu of the Pipeline in Jenkis._

To make the Javadoc load CSS and Javascript, go to Manage Jenkins > Manage Nodes > Settings (gear icon) > click Script console on the left and type in the following command:

```
System.setProperty("hudson.model.DirectoryBrowserSupport.CSP", "")
```

**6)**  Add *Archive* as the last stage of the Pipeline:

*"Archives in Jenkins the archive files (generated during Assemble, i.e., the war file)"*

```
    stage('Archiving') {
        steps {
            echo 'Archiving...'
            dir('CA2/Part2'){
                archiveArtifacts 'build/libs/*.war'
            }
        }
    }
```

**7)**  Finally add *Publish Image* as the last stage of the Pipeline:

*"Publish Image. Generate a docker image with Tomcat and the war file and publish it in the Docker Hub."*

```
    stage('Docker Image') {
        steps {
            echo 'Creating and publishing Docker Image...'
            script{
                def customImage = docker.build("1191781/devopsswitch2019:${env.BUILD_ID}", "-f CA5/Part2/Dockerfile CA5/Part2")
                    customImage.push('1191781_ca5part2')
            }
        }
    }
```

_The **"-f CA5/Part2/Dockerfile CA5/Part2"** parameter inside the docker.build method allowed me to point Docker to the correct Dockerfile.
This option enabled me to use the file that I had in CA5/Part2 instead of uploading it to the root of the Bitbubucket Repository_

It will be available at Docker Hub as **1191781_ca5part2**.

**8)**  The contents of your Jenkinsfile should look like this:

```
pipeline {
    agent any
    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git credentialsId: 'nfc-bitbucket-credentials', url:'https://nunocrisostomo@bitbucket.org/nunocrisostomo/devops-19-20-b-1191781.git'
            }
        }
        stage('Assemble') {
            steps {
                echo 'Assembling...'
                dir('CA2/Part2'){
                    bat 'gradlew clean assemble'
                }
            }
        }
        stage('Test') {
            steps {
                echo 'Running tests...'
                dir('CA2/Part2'){
                    bat 'gradlew test'
                    junit 'build/test-results/test/*.xml'
                }
            }
        }
        stage('Javadoc') {
            steps {
                echo 'Generating Javadoc...'
                dir('CA2/Part2'){
                    bat 'gradlew javadoc'
                    publishHTML (target: [
                          allowMissing: false,
                          alwaysLinkToLastBuild: true,
                          keepAll: true,
                          reportDir: 'build/docs/javadoc',
                          reportFiles: 'index.html',
                          reportName: "Javadoc"
                        ])
                }

            }
        }
        stage('Archiving') {
            steps {
                echo 'Archiving...'
                dir('CA2/Part2'){
                    archiveArtifacts 'build/libs/*.war'
                }
            }
        }
        stage('Docker Image') {
            steps {
                echo 'Creating and publishing Docker Image...'
                script{
                    def customImage = docker.build("1191781/devopsswitch2019:${env.BUILD_ID}", "-f CA5/Part2/Dockerfile CA5/Part2")
                        customImage.push('ca5part2')
                }
            }
        }
    }
}
```

**9)** Run jenkins.war (download it from here: http://mirrors.jenkins.io/war-stable/latest/jenkins.war and put it in a folder of choice)

Open a terminal and run the following commands:
```
java -jar jenkins.war
```
*This step ignores the initial configuration of the Jenkins platform*

**10)** After authentication, open _**Manage Jenkins > Credentials > System > Global credentials (unrestricted) > Add Credentials**_.

Add your Git Repository credentials using the following options:

**Kind**: Username with password 

**Scope**: Global (Jenkins, nodes, items, all child items, etc)

**Username**: \<Your-Git-Repository-Username\> 

**Password**: \<Your-Git-Repository-Password\> 

**ID**: \<Your-ID-To-Be-Used-In-Jenkinsfile\> 

*In this step I used my Bitbucket credentials, and the ID nfc-bitbucket-credentials, that can be seen in the Jenkinsfile*

**11)** Go back to Jenkins Home and open _**New Item**_.

- Enter the following item name: _CA5Part2_
- Select _**Pipeline**_
- Click _**OK**_
- Select the _**Pipeline**_ Tab
- In _**Definition**_, select _Pipeline Script from SCM_
- In _**SCM**_, select _Git_
- In _**Repository**_ URL, paste your repository URL (https://nunocrisostomo@bitbucket.org/nunocrisostomo/devops-19-20-b-1191781.git)
- In _**Script Path**_: use the path to the Jenkinsfile (CA5/Part2/Jenkinsfile)
- Click _**Save**_

**12)** Go back to Jenkins Home and click the name of your new Pipeline **CA5Part2**. Select the option **Build Now** from the side menu.

_**Your Pipeline should build successfully**_

**13)** At the end of this assignment mark your repository with the tag ca5-Part2.

```
git add README.md Jenkinsfile

git commit -m "Fix #35 CA5 - Part 2 - Adds README.md and updated Jenkinsfile" 

git push

git tag ca5-part2

git push origin ca5-part2
```