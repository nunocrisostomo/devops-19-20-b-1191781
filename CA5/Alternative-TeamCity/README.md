![TeamCity-logo-small.png](https://bitbucket.org/repo/8XqLBk4/images/2469659179-TeamCity-logo-small.png "DevOps - Class Assignment 5")

# DevOps - Class Assignment 5 (CA5) - Alternative #

*"For this assignment you should present an alternative technological solution for the
continuous integration tool (i.e., an alternative to Jenkins)."*

-------------

## **Analysis and Comparison** ##

TeamCity (https://www.jetbrains.com/teamcity/) is a build management tool and automation server that facilitates Continuous Integration developed by JetBrains, which is well known for its IntelliJ IDEA. It runs in a Java environment, usually on an Apache Tomcat server, though it can be installed on both Windows and Linux servers.

TeamCity offers equal support for Ruby, .NET and openstack projects, integrating into both Visual Studio, IDEs like Eclipse and of course IntelliJ IDEA.

It has a free and a paid version. The free version, that was used in this implementation, offers up to 100 build configurations (jobs), 3 build agents and of the features of the commercial version, including access to 393 plugins (https://plugins.jetbrains.com/teamcity) to extend its functionality.

One of the main reasons that I found compelling to use it was its UI design and general ease of use. Comparing to Jenkins, even with the Open Blue Ocean project, it's well ahead both in graphical design as usability and accessability.

The availability of hundreds of plugins is well below the 1500+ of the Jenkins repository but the main ones are there. The Jenkins community is bigger and enable a faster support and response to any questions that may appear during implementation. TeamCity doesn't have this community, mainly because of its commercial license but I found its documentation accessible and well written.

Overall, I found TeamCity learning curve to be shorter than Jenkins. It was easier to me to just start using TeamCity. Jenkins is a little more complex in its structure and usability.

I can see myself considering TeamCity in the future if the project budget allows it.  

-------------

## **Steps taken to complete the assignment** ##

*For the sake of simplicity, some git commands are not shown*

**1)** Download and Install TeamCity from the link below:

```
https://www.jetbrains.com/teamcity/download/#section=section-get
```

_I've used TeamCity for Windows, and during installation, changed the default port from 8080 to 8081 to avoid collision with the Jenkins default port._

**2)**  Open Teamcity in your favorite browser, using the following URL:

```
http://localhost:8081
```

**3)**  Create a **new project** using the following options:

- Click the **Projects** menu
- Click **Create project...**

- Use **From a repository URL**
- **Parent project**: \<Root project\>
- **Repository URL**: Write or Paste you Bitbucket Repository
- **Username**: Write your Bitbucket username
- **Password**: Write your Bitbucket password

- Click **Proceed**

- Accept the default suggestions

- Click **Proceed**

**4)** In **General Settings**, change the following options:

- **Publish artifacts**: _Only if build status is successful_
- **Artifact paths**: CA2\Part1\build\distributions => CA2\Part1\build\distributions

This way the files in the folder distributions will be published after the build. No need to set a Build step to publish the files.

**5)** In this step we're going to add the two steps needed to complete **Part 1 of CA5**.

Add the Build steps using the following options:

- In the **_Build Configuration_** page, click **Build Steps**
- Click **Add build step** to create **Assemble** with:
    - **Runner type**: Gradle
    - **Step name**: Assemble
    - **Gradle tasks**: clean assemble
    - **Gradle build file**: CA2/Part1/build.gradle
    - **Gradle Wrapper**: Check option
    - **Path to Wrapper script**: CA2/Part1
    - Click **Save**

- Click **Add build step** to create **Test** with:
    - **Runner type**: Gradle
    - **Step name**: Test
    - **Gradle tasks**: test
    - **Gradle build file**: CA2/Part1/build.gradle
    - **Gradle Wrapper**: Check option
    - **Path to Wrapper script**: CA2/Part1
    - Click **Save**

You've completed all the steps to build Part 1 of CA2.

To run the Build Process click **Run**

You now have a successful build.

-------------

 **5)** In this step we're going to add the two steps needed to complete **Part 2 of CA5**.
 
 Add the Build steps using the following options:
 
 - Click in **\<Root project\>** and then click in **Devops 19 20 B 1191781** sub-project
 - Click **Create build configuration**
 - Use **From a repository URL**
 - **Parent project**: \<Root project\>
 - **Repository URL**: Write or Paste you Bitbucket Repository
 - **Username**: Write your Bitbucket username
 - **Password**: Write your Bitbucket password
 - Click **Proceed**
 
 - In **Create Build Configuration From URL** use:
    - **Build configuration name**: Part 2 Build
 - Click **Proceed**
 - In the Pop-up click the **Use this** button
 
 - In **General Settings**, change the following options:
    - **Publish artifacts**: _Only if build status is successful_
    - **Artifact paths**: CA2\Part2\build\libs\demo-0.0.1-SNAPSHOT.war                 _(This step is only available after the first successful build!)_
                          CA2\Part2\build\docs\javadoc => CA2\Part2\build\docs\javadoc _(This step is only available after the first successful build!)_
 
 - Click **Build Steps**
 - Click **Add build step** to create **Assemble** with:
     - **Runner type**: Gradle
     - **Step name**: Assemble
     - **Gradle tasks**: clean assemble
     - **Gradle build file**: CA2/Part2/build.gradle
     - **Gradle Wrapper**: Check option
     - **Path to Wrapper script**: CA2/Part2
     - Click **Save**
 
 - Click **Add build step** to create **Test** with:
     - **Runner type**: Gradle
     - **Step name**: Test
     - **Gradle tasks**: test
     - **Gradle build file**: CA2/Part2/build.gradle
     - **Gradle Wrapper**: Check option
     - **Path to Wrapper script**: CA2/Part2
     - Click **Save**

 - Click **Add build step** to create **Javadoc** with:
     - **Runner type**: Gradle
     - **Step name**: Javadoc
     - **Gradle tasks**: javadoc
     - **Gradle build file**: CA2/Part2/build.gradle
     - **Gradle Wrapper**: Check option
     - **Path to Wrapper script**: CA2/Part2
     - Click **Save**
 
 Before adding the Docker related tasks, we first have to add Docker Support to our build. To do this:
 
 - Click in **Devops 19 20 B 1191781**
 - Click **Connections**
 - Click **Add Connection**
    - **Connection type**: Docker Registry
    - **Display name**: Docker Hub
    - **Registry address**: https://docker.io/
    - **Username**: Your Docker username
    - **Password**: Your Docker Password
    - Click **Test Connection**
    - Click **Save**
 - Click **General Settings**
 - Click **Part 2 Build** in _Build Configurations_
 - Click **Build Features**
 - Click **Add build feature**
 - Choose **Docker Support**
 - **Check** _On server clean-up, delete pushed Docker images from registry_
 - Click **Add registry connection**
 - Select your previously created **Docker Hub** connection
 - Click **Add**
 - Click **Save**
 
 Now we're ready to add the Docker related steps.

 - Click in **Devops 19 20 B 1191781**
 - Click **Part 2 Build** in _Build Configurations_
 - Click **Add build step** to create **Docker Image** with:
      - **Runner type**: Docker
      - **Step name**: Docker Image
      - **Execute step**: If all previous steps finished successfully
      - **Docker command**: build
      - **Path to file**: CA5/Part2/Dockerfile
      - **Image name:tag**: 1191781/devopsswitch2019:1191781_ca5part2_teamcity
      - Click **Save**
 
  - Click **Add build step** to create **Docker Image Publish** with:
       - **Runner type**: Docker
       - **Step name**: Docker Image Publish
       - **Execute step**: If all previous steps finished successfully
       - **Docker command**: push
       - **Remove image from agent after push**: Check
       - **Image name:tag**: 1191781/devopsswitch2019:1191781_ca5part2_teamcity
       - Click **Save**     
      
 You've completed all the steps to build Part 2 of CA2.
 
 To run the Build Process click **Run**
 
 You now have a successful build.

-------------

**12)** At the end of this assignment mark your repository with the tag ca5-Part2.

```
git add README.md

git commit -m "Fix #36 CA5 - Part 2 - Adds Jenkins Alternative TeamCity README.md"

git push
```