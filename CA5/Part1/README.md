![jenkins-logo.png](https://bitbucket.org/repo/8XqLBk4/images/581662723-jenkins-logo.png "DevOps - Class Assignment 5")

# DevOps - Class Assignment 5 (CA5) - Part 1 #

*"The goal of the Part 1 of this assignment is to practice with Jenkins using the "gradle basic demo" project that should already be present in the student’s individual repository."*
-------------
## **Steps taken to complete the assignment** ##

*For the sake of simplicity, some git commands are not shown*

**1)** Create Jenkinsfile and open it, paste the code below:

```
pipeline {
    agent any
    stages {

    }
}
```

**2)**  Add *Checkout* as the first stage of the Pipeline:

*"Checkout. To checkout the code form the repository"*

```
    stage('Checkout') {
        steps {
            echo 'Checking out...'
            git credentialsId: 'nfc-bitbucket-credentials', url:'https://nunocrisostomo@bitbucket.org/nunocrisostomo/devops-19-20-b-1191781.git'
        }
    }
```

**3)**  Add *Assemble* as the second stage of the Pipeline:

*"Assemble. Compiles and Produces the archive files with the application. Do not use the build task of gradle (because it also executes the tests)!"*

```
    stage('Assemble') {
        steps {
            echo 'Assembling...'
            dir('CA2/part1'){
                bat 'gradlew clean assemble'
            }
        }
    }
```

**4)**  Add *Test* as the third stage of the Pipeline:

*"Test. Executes the Unit Tests and publish in Jenkins the Test results. See the junit step for further information on how to archive/publish test results."*

```
    stage('Test') {
        steps {
            echo 'Running tests...'
            dir('CA2/part1'){
                bat 'gradlew test'
                junit 'build\\test-results\\test\\*.xml'
            }
    
        }
    }
```

**5)**  Finally add *Archive* as the last stage of the Pipeline:

*"Archive. Archives in Jenkins the archive files (generated during Assemble)"*

```
    stage('Archiving') {
        steps {
            echo 'Archiving...'
            dir('CA2/part1'){
                archiveArtifacts 'build/distributions/*'
            }
        }
    }
```

**6)**  The contents of your Jenkinsfile should look like this:

```
pipeline {
    agent any
    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git credentialsId: 'nfc-bitbucket-credentials', url:'https://nunocrisostomo@bitbucket.org/nunocrisostomo/devops-19-20-b-1191781.git'
            }
        }
        stage('Assemble') {
            steps {
                echo 'Assembling...'
                dir('CA2/part1'){
                    bat 'gradlew clean assemble'
                }
            }
        }
        stage('Test') {
            steps {
                echo 'Running tests...'
                dir('CA2/part1'){
                    bat 'gradlew test'
                    junit 'build\\test-results\\test\\*.xml'
                }
            }
        }
        stage('Archiving') {
            steps {
                echo 'Archiving...'
                dir('CA2/part1'){
                    archiveArtifacts 'build/distributions/*'
                }
            }
        }
    }
}
```

**7)** Run jenkins.war (download it from here: http://mirrors.jenkins.io/war-stable/latest/jenkins.war and put it in a folder of choice)

Open a terminal and run the following commands:
```
java -jar jenkins.war
```
*This step ignores the initial configuration of the Jenkins platform*

**8)** After authentication, open _**Manage Jenkins > Credentials > System > Global credentials (unrestricted) > Add Credentials**_.

Add your Git Repository credentials using the following options:

**Kind**: Username with password 

**Scope**: Global (Jenkins, nodes, items, all child items, etc)

**Username**: \<Your-Git-Repository-Username\> 

**Password**: \<Your-Git-Repository-Password\> 

**ID**: \<Your-ID-To-Be-Used-In-Jenkinsfile\> 

*In this step I used my Bitbucket credentials, and the ID nfc-bitbucket-credentials, that can be seen in the Jenkinsfile*

**9)** Go back to Jenkins Home and open _**New Item**_.

- Enter the following item name: _CA5 - Part 1_
- Select _**Pipeline**_
- Click _**OK**_
- Select the _**Pipeline**_ Tab
- In _**Definition**_, select _Pipeline Script from SCM_
- In _**SCM**_, select _Git_
- In _**Repository**_ URL, paste your repository URL (https://nunocrisostomo@bitbucket.org/nunocrisostomo/devops-19-20-b-1191781.git)
- In _**Script Path**_: use the path to the Jenkinsfile (CA5/Part1/Jenkinsfile)
- Click _**Save**_


**10)** At the end of this assignment mark your repository with the tag ca5-part1.

```
git add README.md Jenkinsfile

git commit -m "Fix #32 CA5 - Adds README and final Jenkinsfile" 

git push

git tag ca5-part1

git push origin ca5-part1
```