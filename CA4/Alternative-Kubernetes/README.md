# DevOps - Class Assignment 4 (CA4) - Alternative - Kubernetes#

*"For this assignment you should present an alternative technological solution for the containerization tool (i.e., an alternative to Docker). The suggestion is to explore Kubernetes. See https://kubernetes.io."*

-------------

## **Analysis & Comparison** ##

Kubernetes is an open-source container-orchestration system, created originally by Google. 

When I used the system, it became automatically apparent that Kubernetes and Docker operate at different layers of the stack, and can be used together. 
Kubernetes and Docker are different technologies that work well together for building, delivering, and scaling containerized apps. 
One can orchestrate several types of containers and VMs, the other is a container technology by itself.

Kubernetes allows to coordinate, schedule, update, upgrade and monitor containers. It facilitates the management of containers easier and reduces de downtime of services in the various stages of development and production.

The "rival" of Kubernetes isn't Docker containers, but Docker Swarm that is the same type of open-source container-orchestration system.

Kubernetes supports higher demands with more complexity while Docker Swarm offers a simple solution that is quick to get started with. 
Docker Swarm has been quite popular among developers who prefer fast deployments and simplicity. 
Simultaneously, Kubernetes is utilized in production environments by various high profile internet firms running popular services.

-------------
## **Steps taken to complete the assignment** ##

**1)** Download the latest kubectl.exe file using the command below and add it to the system PATH:

```
curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.18.2/bin/windows/amd64/kubectl.exe
```

**2)**  Download the Minikube installer from the URL below and install it. Minikube allows to run a single-node Kubernetes cluster inside a VM.

```
https://github.com/kubernetes/minikube/releases/latest/download/minikube-installer.exe
```

**3)** Start Minikube with the Docker driver and check its status

```
minikube start --driver=docker

minikube status
```

**4)** Download the latest kompose.exe file using the command below and add it to the system PATH. It will allow to convert the docker-composer.yml into Kubernetes resources files.

```
curl -L https://github.com/kubernetes/kompose/releases/download/v1.21.0/kompose-windows-amd64.exe -o kompose.exe
```

**5)** Convert the docker-composer.yml file into Kubernetes resources

```
kompose convert
```

**6)** Deploy your Docker containers into Kubernetes

```
kubectl apply -f db-service.yaml,web-service.yaml,db-deployment.yaml,web-deployment.yaml,default-networkpolicy.yaml
```

In a Web Browser access the following URL:

```
http://localhost:8080/demo-0.0.1-SNAPSHOT/
```

**7)** Commit alternative README.md and Kubernetes resource files to repository.

```
git add data/ db/ web/
git add docker-compose.yml README.md

git commit -m "Fix #30 CA4 - Adds Kubernete alternative README.md and Kubernetes resources files"

git push
```