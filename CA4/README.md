![horizontal-logo-monochromatic-white_small.png](https://bitbucket.org/repo/8XqLBk4/images/94424747-horizontal-logo-monochromatic-white_small.png "DevOps - Class Assignment 4")

# DevOps - Class Assignment 4 (CA4) #

*"The goal of this assignment is to use Docker to setup a containerized environment to execute your version of the gradle version of spring basic tutorial application"*
-------------
## **Steps taken to complete the assignment** ##

**1)** Download files from repository and extract it to CA4 folder

**2)**  Open CA4/web/Dockerfile and change the line 17 to:

```
RUN git clone https://nunocrisostomo@bitbucket.org/nunocrisostomo/devops-19-20-b-1191781.git
```

Then line 19 to: 

```
WORKDIR /tmp/build/devops-19-20-b-1191781/CA2/Part2
```

And finally, add below line 19 this: 

```
RUN chmod +x gradlew
```

In the folder CA4 where the docker-compose.yml file is, execute

```
docker-compose build

docker-compose up 
```

In a Web Browser access the following URL:

```
http://localhost:8080/demo-0.0.1-SNAPSHOT/
```

**3)** Publish the images (db and web) to Docker Hub (https://hub.docker.com)

Open a terminal and run the following commands to publish the images in Docker Hub:
```
docker tag ca4_web 1191781/devopsswitch2019:devops_ca4_web

docker push 1191781/devopsswitch2019:devops_ca4_web

docker tag ca4_db 1191781/devopsswitch2019:devops_ca4_db

docker push 1191781/devopsswitch2019:devops_ca4_db
```

**4)** Use a volume with the db container to get a copy of the database file by using the
exec to run a shell in the container and copying the database file to the volume

On a terminal execute the following command:

```
docker exec -it ca4_db_1 /bin/bash
```

Then execute:

```
cp jpadb.mv.db ../data
```

**5)** At the end of this assignment mark your repository with the tag ca4.

```
git add data/ db/ web/
git add docker-compose.yml README.md

git commit -m "Fix #30 CA4 - Adds README and docker related files" 

git push

git tag ca4

git push origin ca4
```